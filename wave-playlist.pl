#! /usr/bin/env perl

# Copyright (c) 2023 Jiří Wolker
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation
# files (the "Software"), to deal in the Software without
# restriction, including without limitation the rights to use,
# copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the
# Software is furnished to do so, subject to the following
# conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
# OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
# HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
# FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
# OTHER DEALINGS IN THE SOFTWARE.

use v5.17;
use strict;
use utf8;

use LWP::UserAgent;
use JSON qw/decode_json/;
use Term::ANSIColor;

binmode STDOUT, ":utf8";

my $playlist_url = 'https://api.rozhlas.cz/data/v2/playlist/day/radiowave.json';

my $ua = LWP::UserAgent->new;
$ua->agent("wave-playlist.pl/0.1");

my $res = $ua->get($playlist_url);
my $obj = decode_json $res->content;

for my $song (@{$obj->{data}}) {
    $song->{since} =~ /T(..:..:..)\+/;
    my $time = $1;

    print "$time ";
    print colored $song->{interpret}, 'blue';
    print ' ';
    say colored $song->{track}, 'bold green';
}
